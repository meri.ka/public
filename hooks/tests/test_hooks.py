# pylint: disable=redefined-outer-name
"""
Tests for hooks
"""
from pathlib import Path

import pytest

from hooks.constants import TEAM_FILE_PATH
from hooks.build import load_raw_team, reformat_roles, consolidate_roles, md_slug

FIXTURES_DIRECTORY = Path(__file__).parent / 'fixtures'


def test_data_matches_schema():
    """
    Fails if the current OpenCraft schema doesn't pass type checks.
    """
    try:
        load_raw_team(TEAM_FILE_PATH)
    except TypeError:
        pytest.fail(f"Team data in {TEAM_FILE_PATH} doesn't match schema.")


def test_transform_roles():
    """
    Tests that roles are transformed from a set of positions to an ordered set
    of positions.
    """
    result = reformat_roles({
        'Sprint Manager': {
            'primary': ['Joe', 'Curly', 'Moe'],
            'backup': ['Fiona'],
        },
        'Sprint Planning Manager': {
            None: ['Shrek', 'Donkey'],
            'primary': ['Gingerbread Man', 'Dragon'],
        },
    })
    assert result == [{'name': 'Sprint Manager',
                       'positions': [{'members': ['Joe', 'Curly', 'Moe'], 'position': 'primary'},
                                     {'members': ['Fiona'], 'position': 'backup'}]},
                      {'name': 'Sprint Planning Manager',
                       'positions': [{'members': ['Shrek', 'Donkey'], 'position': None},
                                     {'members': ['Gingerbread Man', 'Dragon'],
                                      'position': 'primary'}]}]


def test_team_roles():
    """
    Test that company-wide roles are recognized and consolidated.
    """
    raw_team = load_raw_team(FIXTURES_DIRECTORY / 'test_roles.yml')
    result = consolidate_roles(raw_team)
    assert result['team_roles'] == {
        'Community Liaison': {None: ['Batman'], 'backup': ['Hunter the Cat']},
        'Developer Advocate': {None: ['Batman', 'Hunter the Cat']},
    }


def test_cell_roles():
    """
    Test that cell roles are recognized and consolidated.
    """
    raw_team = load_raw_team(FIXTURES_DIRECTORY / 'test_roles.yml')
    result = consolidate_roles(raw_team)
    assert result['cell_roles_by_name'] == {
        'Destroyer': {
            'DevOps Specialist': {'backup': ['Hunter the Cat'], 'primary': ['Kylo Ren']},
            'Epic Planning and Sustainability Manager': {'delegated': ['Inspector Gadget']},
            'Sprint Planning Manager': {None: ['Batman']}
        },
        'Dreadnaught': {
            'Sprint Manager': {None: ['Walter White']},
        }
    }


def test_clear_inline_developer_roles():
    """
    Test that inline roles for non-support cells are cleared.
    """
    raw_team = load_raw_team(FIXTURES_DIRECTORY / 'test_roles.yml')
    result = consolidate_roles(raw_team)
    for member in result['cells_by_name']['Destroyer']['members']:
        assert member['roles'] == []


def test_preserve_inline_support_roles():
    """
    Test that inline roles are preserved for support cell members.
    """
    raw_team = load_raw_team(FIXTURES_DIRECTORY / 'test_roles.yml')
    result = consolidate_roles(raw_team)
    all_roles = {}
    for member in result['cells_by_name']['Dreadnaught']['members']:
        all_roles[member['name']] = member['roles']
    assert all_roles == {'Frankie the Cat': [{'name': 'UX Designer', 'scope': 'company'},
                                             {'name': 'Product Manager', 'scope': 'company'}],
                         'Inspector Gadget': [{'name': 'Cell Supporter', 'scope': 'company'}],
                         'Walter White': [{'name': 'Business Development Specialist',
                                           'scope': 'company'}]}


def test_reformat_roles():
    """
    Test the role formatter to verify it sorts roles into order for the template.
    """
    result = reformat_roles({
        'DevOps Specialist': {'backup': ['Hunter the Cat'], 'primary': ['Kylo Ren']},
        'Epic Planning and Sustainability Manager': {'delegated': ['Inspector Gadget']},
        'Sprint Planning Manager': {None: ['Batman']}
    })
    assert result == [
        {
            'name': 'DevOps Specialist',
            'positions': [
                {'members': ['Kylo Ren'], 'position': 'primary'},
                {'members': ['Hunter the Cat'], 'position': 'backup'},
            ],
        },
        {
            'name': 'Epic Planning and Sustainability Manager',
            'positions': [
                {'members': ['Inspector Gadget'], 'position': 'delegated'},
            ],
        },
        {
            'name': 'Sprint Planning Manager',
            'positions': [
                {'members': ['Batman'], 'position': None},
            ],
        }]


@pytest.mark.parametrize("value,expected", (
    ('I am going to the store today.', 'i-am-going-to-the-store-today'),
    ('I hope they have cake (not just pie)', 'i-hope-they-have-cake-not-just-pie'),
    ('Pie is good, but cake is really &RT#$)in good.', 'pie-is-good-but-cake-is-really-rtin-good'),
))
def test_md_slug(value, expected):
    """
    Test our markdown-id slugifying filter.
    """
    assert md_slug(value) == expected
