# Junior developer role

During the trial of a [newcomer](#newcomer), we sometimes come across a profile that is very promising, but where a _lack of experience_ is making them just short of matching the expectations for a core team developer. Rather than refusing the person outright, in some of those cases we will offer a "Junior developer" role instead. The goal is to allow the person to gain that missing experience, by assigning a specific role that provides time to learn, and is not under the constant pressure of a trial period. 

The scope of tasks given to a junior developers is different, it doesn't draw from the same pool of work as the cell's or newcomers in trial. It is more akin to sponsoring the junior developer to learn through immersing themselves within the Open edX community, and working on upstream contributions to Open edX that aren't tied to a specific client timeline, but still helps advancing OpenCraft's priorities.

Note that compared to the trial, this is long term status, intermediary to the core team member status. It is thus meant for profiles who are _only_ missing experience -- all the [other qualities required for core team members](./processes/onboarding_and_trial_period_evaluation.md#evaluation-criteria) should be present. So if there are other issues than ones directly caused by a lack of experience, a junior developer role isn't an appropriate alternative to a trial extension, or to simply saying 'no'. Junior developers would for example gain more permissions and privileges than newcomers, and we want to pick profiles who will be joining the team for the long haul. 

## Types of tasks

The type of tasks chosen are meant to allow for a low level amount of pressure in terms of timing -- the deliverables aren't tied to specific delivery promise we made to clients or to the community, so a missed deadline won't endanger a project or send the epic owner scrambling to find a way to recover. At the same time, the tasks being upstream contributions to Open edX, it means junior developers will still have a recipient for the work that cares about it (the community - including us!), who will have high expectations in terms of quality and careful reviews. It will contribute to expose junior developers to skilled reviewers and to the Open edX project.

Note that identifying and specifying tasks will require work. This is something the mentor will have to dedicate time to, but also the junior developer themselves, by finding work that's useful within the community and OpenCraft. Junior developers won't have to deal with clients, but being able to find useful work within the community and OpenCraft is an important aspect of the social & specification experience to learn as part of the role, at the contact of a friendly community -- but which ultimately the junior developer is responsible to identify.

### Primary tasks: Core Contributors delegations & priorities

The tasks assigned must match all the following requirements:

* Be an *upstream* contribution to the Open edX project
* Be a core contributor task delegated by an OpenCraft core contributor, and which will help guaranteeing the 20h/month commitment of that core contributor, and match the "Priorities for Core Contributor Work" from the [core contributor program epic](https://tasks.opencraft.com/browse/SE-2700). 
* Not have a close client deadline associated to it - ideally, no client deadline, and at the minimum, there should be at least 3 sprints to complete it. The goal remains that each task be completed within 1 sprint, but this allows to handle the worst case scenario gracefully, even when a task spills over to the second sprint, and needs to be reassigned to someone else for the third sprint.

This should be the main source of work, the secondary tasks only provide occasional filler tasks to give time to prepare more primary tasks, in collaboration with the OpenCraft core team members. A dedicate task to discuss, find and specify this type of work should be scheduled for each sprint, for both the mentor and the junior developer.

Note that for the core contributors tasks delegation, the idea is to help matching our hours quotas, by allowing to delegate some of the 20h/month quota when the core contributor can't provide all the necessary hours. It is likely that the junior would take more time to do the work, and that we would end up exceeding our time commitment this way; the point would be to provide the equivalent work to the 20h that the core contributor would provide, as well as its quality through the core contributor's involvement in the specification and review of the work.

### Secondary tasks: Community 

If not enough delegated core contributor tasks can be found to fill the junior developer's sprint, alternatively the mentor can pick a task or project from the following community boards (by decreasing priority):

* [Release group board](https://github.com/openedx/build-test-release-wg/projects/1)
* [INCR project](https://openedx.atlassian.net/projects/INCR/issues/INCR-4?filter=allopenissues) 
* [Community group board](https://github.com/openedx/community-wg/projects/1)
* [Core contributors program board](https://github.com/orgs/edx/projects/1) 
* [Other community boards](https://github.com/orgs/edx/projects)
* [Open edX roadmap](https://github.com/orgs/openedx/projects/4/views/1)
* [TCRIL board](https://github.com/orgs/openedx/projects/8/views/1)

See also a [list of Open edX issues boards](https://github.com/orgs/openedx/projects/1).

Before taking one of these tasks, make sure to check first with the epic owner of the [core contributor program epic](https://tasks.opencraft.com/browse/MNG-2649) that there is no other core contributor work from the priorities that could be taken, and that the community task taken is adequate. Also check ahead of time on the community task itself that it is still up for grabs, current, accurate and sufficiently described -- make sure to have explicit confirmation on the task from the upstream who will be reviewing it before scheduling the work.

### Optional tasks: Normal core developer tasks

Occasionally, with the agreement of the junior developer, their mentor, and the affected epic owner, the junior developer can take a normal task from their cell. This can be used as a way to check from time to time how the junior developer fares on the tasks they will eventually be expected to take, and how close they are from matching the requirements of a core team member. This would also increase the diversity of work and experience gained, as well as help providing extra hours towards the cell's workload, when appropriate.

Also, to gain experience, in particular reviews can be helpful - junior developers can take them, as long as the task itself is either assigned to a core team member, or has a core team member as the first reviewer (the junior developer is then a second reviewer).

## Task difficulty & complexity

Task difficulty & complexity need to be considered while picking tasks. There is no specific prescription though, it mainly needs to be tasks both the junior developer and the mentor think are a good fit. Since one of the goals of junior developer positions is learning and progression towards the core team member role, it is useful to pick tasks that are _slightly_ difficult for the junior developer, but not insurmountable, so that there is something to learn and progress without being blocked too often.

At the very beginning, it can be worth starting small -- even with non-code contributions, which can even be good first ones. They can often be a good way to learn the ropes of the upstream contribution process, and start establishing relationships with the reviewers. Then the complexity of the tasks should increase over time, and once the contribution process is mastered, to focus more on code, as gaining experience in that area is going to be one of the keys to be able to integrate the core team.

Also, contributions tasks aren't limited to small tasks. Contributions can be big -- or even entire projects/features/epics. To gain experience, the junior developer should eventually demonstrate the ability to take on and successfully complete such larger works.

## Mentoring

A junior developer keeps a mentor assigned, which can be the same person or a different one as the mentor assigned during the initial onboarding period. The mentor has the same duties as described in the [mentor role](../roles.md#mentor) for newcomers, with an additional responsibility to:

* Find and shape the tasks to assign to the junior developer; unlike for newcomers in their trial, the tasks are designed and assigned to the junior developer, rather than picked by the junior developer from the backlog.
* Set timeboxes on the tasks, ensure they aren't exceeded, and review/approve the need to extend the timebox _before_ more time can be logged by the junior developer; and if a task gets "stuck" and doesn't show reasonable results, to change the task.
* Report to the cell on the progress and accomplishments of the junior developer, and schedule a review when appropriate

## Reviews

The mentor reports regularly on the progress of the junior developer, towards the goal of becoming a full core team member. This should happen on the dev@ mailing list, used for review, at least once every 3 months - though more frequent updates are welcomed.

Whenever the mentor thinks the junior developer has done enough progress, the mentor triggers the start of a new trial period, and the junior developer becomes a newcomer in trial again, with the same role, tasks, and review decision at the end -- including the possibility to revert to junior developer if more experience is still needed.

## Budget and availability

The budget for the tasks from junior developers are either:
* In case of a core contributor task delegation (primary task) or a community task (secondary task), assigned to a dedicated non-cell "Junior developer contribution" budget, common to all junior developers.
* In case of a normal cell task (optional tasks), assigned to the normal budget for the task.

Given the non-cell budget, the budget for each new junior developer is approved individually by the CEO, during the trial review.

Since the work of the junior developer contributes only occasionally to the cell availability towards its commitment, the junior developer's availability is not included in the availability totals of the cell -- instead, it participates as an additional buffer.

## Other

Note that all points from the [newcomers role](../roles.md#newcomer) apply. The only point which doesn't apply is logging time on the onboarding epic -- this would only apply whenever the newcomer takes a normal core developer task (see above); for the other types of tasks described above, all time is logged on the task itself. This is also true for 121 mentoring sessions, which also use the "Junior developer contribution" account for the meeting task.

Also, all other points which apply to other cell members apply to the junior developer -- for example, posting a video sprint update and watching the updates from other cell members, participating in the other company processes (retrospective, checklist, forum threads, …). be reviewers of tasks, in particular tasks in epics that deal with upstream (upstreaming changes, edX work, …). These are "normal" cell tasks, ie of the 3rd optional category listed above, and are logged on the normal cell accounts rather than on the dedicated junior one.
