# Blog

## Writing in the [OpenCraft Blog](https://opencraft.com/blog)

Members of the OpenCraft team publish blog posts on our website. Such blog posts provide news about OpenCraft, updates about the Open edX platform, technical tutorials, and other news. If you have an idea and would like to prepare and publish a blog post, you are welcome to!

### Who at OpenCraft is responsible for maintaining the blog?

The [Marketing Specialist](../roles.md#marketing-specialist) (currently Gabriel D'Amours) is responsible for maintaining the blog. He publishes blog posts as per a specific [schedule](https://docs.google.com/spreadsheets/d/1a3aFLNDAa4memApndfcAPlxMqq027rmcVSR4uWnNrpA/edit#gid=661028793).

Contact him if you have any questions!

### Blog Publishing Process

The following is the procedure to publish an article on the [OpenCraft Blog](https://opencraft.com/blog).

#### Share your idea

Have an idea for a blog post? Create a public thread on the [OpenCraft Discussion Forum](https://forum.opencraft.com/), draft a small brief for your blog post, and ping the Marketing Specialist. There is no formal approval process, but team members might request that you refine your idea.

#### Collaborative blog posts

We will sometime publish blog posts in partnership with people outside of OpenCraft. For example, we [co-publish](https://opencraft.com/blog/open-edx-core-committers-program/) articles about projects we've done in collaboration with Open edX, and the blog post will appear both on our blog and Open edX's blog. Collaborative blog posts require additional research, preparation work, and budget.

#### Create a ticket

Once ready:

* Create a ticket to prepare the blog post. The epic should be [OC-5152](https://tasks.opencraft.com/browse/OC-5152), and the Account should be [*Marketing*](https://tasks.opencraft.com/secure/TempoAccount.jspa?key=OPENCRAFT-MARKETING).
* Find a reviewer
* Assign the Marketing Specialist as 2nd reviewer

For a standard blog post, the budget for the ticket should be **10h**. The budget is roughly distributed as follows:

* 6h for preparing the blog post (assignee)
* 3h for the review process and proofreading (reviewers, assignee)
* 1h for publishing (Marketing Specialist)

For a collaborative blog post, the budget for the ticket should be **15h**. The budget is roughly distributed as follows:

* 4h for coordinating with the other author(s)
* 6h for preparing the blog post (assignee)
* 4h for the review process and proofreading (reviewers, assignee)
* 1h for publishing (Marketing Specialist)

#### Write a draft

Write a draft in a Google Doc. Make sure the doc is in the following [folder](https://drive.google.com/drive/folders/1XZuCMpp6_3zowZJCE2uKDB_kmdtB1ycM?usp=sharing), and set permissions so that any member of OpenCraft can edit the doc.

Once ready for review, ping your reviewers.

#### Proofreading Process

Once the draft has been reviewed and the comments addressed, it's time to have your copy proofead. Follow the steps from [proofreading procedure](proofreading.md) in the Handbook to have your copy proofread.

#### Publication

It's now time to create the blog post in Wordpress.

* Log into https://opencraft.com/login (if you don't have an account, ask the Marketing Specialist to create one for you)
* Go to Posts -> Add New
* Add a title
* Start the post with *This article was written by team member [name] (github link)* -> [example](https://opencraft.com/blog/a-new-editable-gradebook-for-open-edx/)
* Paste your copy. Headings will be used to generate a table of contents -> [example](https://opencraft.com/blog/a-new-editable-gradebook-for-open-edx/)
* In the right-hand vertical menu:
    * Pick a Category
    * Add relevant Tags
    * Set a Featured Image. This image will be used as the cover image for your article. Make sure you have permission to use the image ([Unsplash](https://unsplash.com/)) is great for this)
    * Attribute credits for the image at the end of your post
* Save draft and ping the Marketing Specialist on your Jira ticket when ready. The Marketing Specialist will confirm when the article is published. 

**Do not publish the post! The Marketing Specialist will do it, as per the [schedule](https://docs.google.com/spreadsheets/d/1a3aFLNDAa4memApndfcAPlxMqq027rmcVSR4uWnNrpA/edit#gid=661028793).**

#### Sharing the good news with the Community

Once the Marketing Specialist has confirmed that the article is published, share the blog post with the Community by creating a thread in the [Community - News](https://discuss.openedx.org/c/community/community-news/35) category on the official Open edX forum. Write a very brief description of your article, and share a link to the blog post. 

Congratulations, you have written and published a blog post for OpenCraft : )

#### SEO process (optional but recommended)

Our blogging platform in Wordpress uses a Search Engine Optimization (SEO) tool called [Yoast](https://yoast.com/) to review your copy and suggest changes that will increase the searchability and placement of your article on search engines.

Once your copy is in Wordpress, the Yoast tool will automatically review the copy and suggest SEO improvements. The suggestions can be found in the post creation tool, in the right-hand vertical menu, under "SEO Analysis".

We encourage you to take the suggestions into accounts, and do the necessary changes to your text to improve SEO readability. You can ping your reviewers for one last quick review after this, if necessary.
