# How to enable custom HTML certificates

Purpose: Certificates are the diplomas that learners receive when they complete a course. Here's how to change the design.

- You don't need to run commands from command line, like ./manage.py. This is only needed for PDF certificates, which we want to discourage. You can do everything from the admin
- Enable some variables to `FEATURES`:

   ```yml
   CERTIFICATES_HTML_VIEW: true

   CUSTOM_CERTIFICATE_TEMPLATES_ENABLED: true
   ```

   Note that these are enabled by default on OCIM. Only need to remove
   `SANDBOX_ENABLE_CERTIFICATES: false` from the
   `configuration_extra_settings`.

  - `CERTIFICATES_ENABLED` is no longer required
- See [Course Certificates](https://docs.google.com/document/d/1SJa_MnISta7DiTIrR_dbji1Y1-U_v7AyAt_mTJygdU4/edit)
  shareable doc for steps that client can do.
- Add a *course mode* for your course. This can be done from http://localhost:8000/admin/course_modes/coursemode/
  - Check [Django Administration » How to Create a Course Mode](https://docs.google.com/document/d/1NvcUTnlNJqjkIY428baV4jV2BO4pVY4ke2s7R4dXhBk/#heading=h.1d52pzy9hhcs) for detailed instructions
  - The setup we recommend is to keep your course in "honor" mode and later change the template to make it look like the "verified", which looks nicer. This is done through the `course_mode_class` variable in the [base HTML template](templates/html_certificate.html) we created
- In http://localhost:8000/admin/certificates/certificategenerationconfiguration/ you'll need to add one object (if not already there) to enable it
- In http://localhost:8000/admin/certificates/certificatehtmlviewconfiguration/ you'll need to add on object with the JSON configuration. Copy/paste it from [the documentation](http://edx.readthedocs.io/projects/edx-installing-configuring-and-running/en/latest/configuration/enable_certificates.html#configure-certificates-for-your-open-edx-instance) and change names as needed
  - If you get *Database returned an invalid value in QuerySet.dates(). Are time zone definitions and pytz installed?*, see
    https://stackoverflow.com/a/21571350/4302112 for the workaround.
- In http://localhost:8000/admin/certificates/certificatetemplate/ you'll need to add one object with your template code
  - See detailed instructions at [Django Administration » How to Edit a Certificate’s HTML Template](https://docs.google.com/document/d/1NvcUTnlNJqjkIY428baV4jV2BO4pVY4ke2s7R4dXhBk/edit#heading=h.mvytcg672v64)
  - We created [this base HTML template](templates/html_certificate.html) that you can use there so that the clients can change blocks easily. It was created starting from [open-release/ginkgo.1/lms/templates/certificates/valid.html](https://github.com/edx/edx-platform/blob/open-release/ginkgo.1/lms/templates/certificates/valid.html) and expanding the includes.
- After these changes, enter to Studio, and in *Settings → Certificates* you should see options to create one. Create it. Then you'll see a *Preview* button on the top right
