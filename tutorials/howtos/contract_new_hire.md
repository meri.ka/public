# HOWTO - Backup role for recruitment - Interviewing & establishing a framework contract for a new team member

The process of hiring a newcomer is shared between the Recruitment Managers, the Admin Specialist, and the CEO. This HOWTO details the steps handled by the Admin and the CEO, once a candidate has passed their first (technical) interview. 

## Recruitment workflow & checklist

Complete the tasks listed for the Admin in the [recruitment workflow](https://opencraft.monday.com/workspaces/375051), recurring each sprint.

## Candidate selection (Admin)

One of the tasks in the workflow is to review all [candidates](https://docs.google.com/spreadsheets/d/1kP1M-cfXzwSJmZ3i2VnQnMwjPhXJmLQD8Xtyr7woOpA/edit#gid=1024882121&fvid=1056367567) who have the status "Interview - Yes (reviewed)". Check the existing comments from recruitment managers, and become familiar with the candidate profile. No need to do any specific review, the recruitment managers already checked that the profile would match -- though of course you keep the responsibility, like everyone involved, to refuse a candidate at any time (1 "no" is enough to disqualify a candidate).

## Candidate 2nd interview invitation (Admin)

Schedule an interview with the candidate, using the following template, and cover the points mentioned in the email's agenda:

> Hi FIRSTNAME,
> 
> NAME at OpenCraft here. Thank you for meeting with RECRUITMENT-MANAGER! And congratulations, you have passed the technical interview, and you are now advancing to the next round. This round, you will be meeting with our CEO Xavier, and the discussion will focus more on the personal, cultural and contractual aspects.
> 
> Here are some of the points we'd like to cover during this next meeting:
>  * Learning more about you - your story until now, and in particular your relationship to open source
>  * Whether you have questions about our way of working, and checking with you that we are a good fit for you. We work a little differently than most companies, so it's worth reviewing this together. Could you read our handbook at https://handbook.opencraft.com/ before our meeting, and write down any question you have, so we can discuss them at that time? We'll be happy to clarify anything that the handbook doesn't cover well.
>  * Answer any questions you might have about our standard contract, which I attach. During the meeting, we'll also need to confirm your hourly rate (in euros). Since remuneration is based on time spent working, be sure to factors in all costs, including taxes, vacation and benefits.
>     - Note that we need it now to take a decision for the next step, so please make sure that it's your final rate. We won't try to negotiate - we prefer to work with you at the rate you want, not any lower. We'll need to know your final rate to check if it is within our budget, and you won't be able to change it later on in the process.
>     - An important point is that this is *not* meant as a race to the bottom, but part of a process to keep things as fair as possible. See how we handle remuneration in our handbook: https://handbook.opencraft.com/en/latest/team_compensation/
>     - We know that you may not know how to best price yourself, so we have built a tool to help you determine your rate. Make a copy of this spreadsheet and take the time to fill it in as completely and thoroughly as you can. We will not ask you for the spreadsheet or its results, but given what you enter, it will suggest a rate for you: https://docs.google.com/spreadsheets/d/1XI47TfbWo623ZKT-XfChKu-lX6QlxbcdH0xgTIkRqdY/
> * To book the meeting, please use CALENDLY-LINK
> 
> Best regards, 

## 2nd interview (CEO)

Conduct an interview with the candidate.

* Check if:
    * They show a real interest for open source, as well as an history of contributing by collaborating with others
    * They have any pending questions about how OpenCraft works, or about their contract -- answer them, and verify that they don't have a significant blocker or incompatibility
    * Their hourly rate is within the acceptable range (don't react about it during the meeting though, to avoid negotiations)
* Confirm:
    * Their starting date: the first day of the next sprint the newcomer is available - try to keep the delay as short as possible, including asking to negotiate the notice period with their employer, if it is long
    * Their weekly volume of hours
    * That any outstanding question or gap in the application details spreadsheet for this candidate have been resolved & added to the spreadsheet notes

If all is good, then move on to contract signature.

## Contract signature (CEO)

Send the following email (with the Admin Specialist **in CC**):

> Hi FIRSTNAME,
> 
> Glad to have met you (/TIME)! :) And congratulations, we all think you are a perfect candidate for the job, and we would be happy to welcome you to the team!
> 
> To finalize the contractual part, I will send you the contract through Hellosign. If everything looks good, fill the fields (name & address of the entity you will use in invoices, start date, etc.) and sign -- I'll then sign in turn.
> 
> If you have any new question let me know.
> 
> Hoping to see you on the team soon! :)

Then:
* Go to [HelloSign](https://app.hellosign.com/) and login
* Click "Sign documents" on the top right
* Select "Add template" -> "Framework contract", and click "Next"
* Fill the contacts form:
    * Provider: Full legal name & personal email of the new team member
    * Founder/CEO: Xavier Antoviaque - xavier@opencraft.com
      * **Important note:** If Xavier is on vacation, use billing@opencraft.com instead of Xavier's email. Otherwise you won't be able to sign the contract since you don't have access to Xavier's inbox. 
    * Keep "Set signer order" selected, then click "Next"
    * Fill "Individual name", "Hours per week" and "Hourly rate"
    * Add the name of the candidate to the "Document title": "Framework Contract - First Lastname"
    * Click "Send for signature"

First the newcomer will need to sign, then Hellosign will ask you to sign in turn:
* Make sure the address is full and correct, and corresponds to the entity that will invoice OpenCraft
* Check the start date
* Sign
* Save the contract on the drive

Once the contract is signed by both parties, send the following email to the candidate, and add the recruitment manager in CC so they can take over:

> Hi FIRSTNAME,
> 
> Thanks for signing! I will sign it in turn, you should get an executed copy from Hellosign. 
> 
> I'm looping RECRUITMENT-MANAGER back in, who will send you more details before your start date on START DATE.
> 
> In the meantime, if you would like to get started, have a look at the onboarding course: https://courses.opencraft.com/courses/course-v1:OpenCraft+onboarding+course/course/ - especially, setting up your development environment will gain you a lot of time when you start. Please take note of the time spent reading the onboarding course -- you'll be able to log it later in Jira.  
>
> Welcome to the team, and please don't hesitate if you have any questions!

## Onboarding (Admin)

Once the contract is fully executed, i.e. both sides have signed, start the onboarding -- this should ideally be done ahead the Thursday before the beginning of the sprint the newcomer starts, to give time to the different roles involved to complete the onboarding and create the initial tasks.

* Go to the [onboarding checklists directory on the drive](https://drive.google.com/drive/folders/1iKqvp7YvXIY18AGRkijAvGWXfLKaS1-Y)
* Create a new blank checklist for the newcomer, using the template: "New" -> "Google Doc" -> "From a template" -> "Create" -> "_template Onboarding Checklist"
* Complete the tasks for "Admin" at the top of the "Onboarding" checklist.

## Rejection email (CEO)

If for some reason the candidate doesn't work out, here is a rejection template:

> Hi FIRSTNAME,
> 
> Thank you again for meeting with me - it was a pleasure to talk to you.
> 
> I have just finished reviewing the candidatures; I'm sad to say that I will not be retaining your application for the next round. Please do not take it personally - there was a lot of strong profiles, and we do sometimes make mistakes.
> 
> In any case, I wish you good luck in your job search, and I hope you end up finding a good position!

Generally only provide more feedback if the candidate asks for it, it keeps the discussions simpler in most cases - not everyone seem to like to know why...
