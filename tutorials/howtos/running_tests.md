# How to run the many different types of tests in Open edX

Purpose: Here are instructions and examples for running the test suites on various parts of Open edX.

See the [official documentation about tests](https://github.com/edx/edx-platform/blob/master/docs/testing.rst) for an introduction and the latest syntax for each type of test.

Here we list what we use the most and add some tips.

## Tests after PR

When you create a pull request against some repositories (like `edx/edx-platform`), automatic tests will run and you will find the results linked from the github PR page.
They do this with Jenkins through [this configuration](https://github.com/edx/testeng-ci).
Tests for `edx-platform` take about 2 hours!

You can run the same tests in local, and you should run as many as you can before you push, to get to a clean build faster.

## Python tests

Run all of them, fast:

```bash
paver test_python --disable-migrations --fasttest
```

To run only some of them, you can specify the file to test, the class, and even the method, through a command like this (this is *pytest* syntax):

```bash
paver test_system \
        --fasttest \
        --disable-migrations \
        --pdb \
        -t common/djangoapps/enrollment/tests/test_emails.py::EnrollmentEmailNotificationTest::test_email_sent_to_staff_after_enrollment
```

Don't miss `--pdb`, it will open the debugger when a test fails.

To test djangoapps that don't live under lms/cms but rely on settings from these systems, use this trick:

```bash
paver test_system \
  --fasttest --disable-migrations \
  -t lms/../openedx/features/announcements/tests
```

## Python code quality tests (e.g. pylint, pep8)

```bash
paver run_quality
```

See http://edx.readthedocs.io/projects/edx-developer-guide/en/latest/testing/code-quality.html

## JavaScript tests (defined in `..._spec.js` files)

```bash
paver test_js_run -s lms
paver test_js_run -s xmodule
etc.
```

They run fairly fast, 2 or 3 minutes for all.

## JavaScript code quality tests (e.g. eslint)

```bash
paver run_eslint > ~/eslint_report.txt
```

It's expected to see thousand of errors from old existing code. You should fix the errors in files affected by your changes.

## bok-choy (UI tests, with Selenium)

Example syntax:

```bash
paver test_bokchoy -t lms/test_lms_problems.py::LogoutDuringAnswering::test_logout_after_click_redirect --fasttest
```

These tests can be slow! E.g. more than 10 minutes for a single test.

Tips to speed them up:

- you can also speed those up for development by running `--servers-only` in one shell, and `--tests-only` in another shell.  Then as long as you're only modifying the test code, you don't have to restart the servers every time.

Selenium test might require a specific Firefox version, e.g. Firefox 42. To test with a different version of firefox, this might help:

1. Download and un-tar the firefox version. Here's how you do it for Firefox 42, into your `src` directory:

        # Execute this from edx-platform root as the edxapp user, and it downloads into your src directory.
        curl -o src/ff42.tar.bz2 -L
        https://ftp.mozilla.org/pub/firefox/releases/42.0/linux-x86_64/en-US/firefox-42.0.tar.bz2
        tar -xjf src/ff42.tar.bz2

2. Set the environment variable `SELENIUM_FIREFOX_PATH`:

        export SELENIUM_FIREFOX_PATH=src/firefox/firefox

3. Run your tests as usual

        paver test_bokchoy -t foo/bar.py:MyTest.test_hurrah

4. To switch back to firefox 28, unset the path:

        unset SELENIUM_FIREFOX_PATH

## Coverage tests

Described in [How Test Coverage Works](https://openedx.atlassian.net/wiki/spaces/TE/pages/9076784/How+Test+Coverage+Works).

## Ocim tests

[Ocim](https://github.com/open-craft/opencraft/) uses a different syntax, with commands like `honcho -e .env run ./manage.py test`. See Ocim's [documentation](https://github.com/open-craft/opencraft#running-the-tests) and its [`Makefile`](https://github.com/open-craft/opencraft/blob/master/Makefile).

## Adding new tests

As part of the code you develop, you'll need to add corresponding tests too. Which type of tests are required depends on the features you're adding. In general, find a similar feature, then find its tests, then add new ones in a similar way.
Check the [onboarding course](https://courses.opencraft.com/courses/course-v1:OpenCraft+onboarding+course/about) for information about how to develop good tests.

## Load Tests

Load tests refers to code that automatically does a series of requests to a running server. The server's performance is then analyzed to see how the load was handled.
edX uses [locust IO](http://locust.io) to run load test. Locust exposes a very [simple API](https://docs.locust.io/en/stable/quickstart.html) to run requests and to distribute test running across multiple processes and/or boxes.

To run load test against an edX instance, clone [edx-load-tests](https://github.com/edx/edx-load-tests) repo and follow these steps:

1. Make sure that `AUTOMATIC_AUTH_FOR_TESTING` is set to `True`:
```json
{
    "FEATURES": {
        "AUTOMATIC_AUTH_FOR_TESTING": true
    }
}
```

2. Build the tests which will be run. Building  test will install the necessary requirements and create a settings file.
```bash
$ make <test-name>
```
> *Note:* Preferably use a virtualenv to build tests.

> *Note:* `test-name` is the directory name under `loadtests/`

> *Note:* To run load tests against the local devstack set the `LT_ENV`:

```bash
$ LT_ENV=devstack make <test-name>
```

3. Edit settings file created:
```bash
$ vim settings_files/<test-name>.yml
```
We have to create an object with specific details for the course we're going to test against. To do this follow these next steps:

- Open `helpers/course_data/__init__.py` and copy the example `demo_course` object.
- To update the details in there we have to export the course we're going to use for load testing.
  - Go to the course outline in studio then click on Tools and then on Export.
  - Download the tarball and extract it locally.
- Add the name of every xml file under `sequential` to the `sequential_ids` parameter.
- Add the name of every xml file under `problems` to the `capa_problems` parameter. And base the test answers on the data from the example course data object.
- Add the name of every (if any) xml file under `video_modules` to the `video_module_ids` paramter.
- Add the name of every (if any) xml file under `videos` to the `video_ids` paramter.
- Add every `url_name` value in `course/course.xml` to the `courseware_paths` paramter and complete the url with the values in `chapters/`.
- Add testing special exam ids (if any) to the `special_exam_ids` parameters as a tuple. The ids should be integers, e.g. `special_exam_ids=(21, 22)`

> *Note:* Make sure the course id referred to in the settings exists.
> Also, make sure the login URL is correct, e.g. `LOGIN_PATH: '/user_api/v1/account/login_session/'`

4. Run locust:
```bash
$ locust --host=<server-url> -f loadtests/<test-name>
```
> *Example:* To run against local devstack use `--host=http://localhost:18000`.

5. Go to locust web UI: http://localhost:8089

6. Fill the form and run tests.
