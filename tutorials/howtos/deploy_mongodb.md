# Deploy or Upgrade MongoDB for Open edX

This tutorial describes how to deploy/upgrade different types of MongoDB deployments.

Tickets where we did this [FAL-871], [FAL-872], [FAL-1777] (internal links)

## Risks

### Open edX compatibility

To ensure that the version of Open edX you're running is compatible with the MongoDB version you deploy, check the
[openedx_native.yml] playbook for your version of Open edX.  The major version used for native deployments will be shown
there, and so any minor version within that major version will be compatible.

For example, [Koa supports mongodb 3.6][koa-mongodb], and [lilac supports mongodb 4.0][lilac-mongodb].

### Outage during upgrade

If you're running a single-instance mongodb service, then users will experience an outage during the upgrade for Open
edX course data in LMS and Studio, and the discussion forum.

If you're running a replicaset cluster, then the nodes can be upgraded without outage.

Be sure to decide on a maintenance window and notify your users accordingly.

### Data loss

Upgrading a service in place may result in data loss or corruption.

Be sure to take backups before beginning an upgrade. See [MongoDB Backup Methods][mongodb-backup] for details.

## Deploy new service

If you want to deploy a new service, or deploy a new node from scratch to an existing cluster, follow these steps.

1. Create a new ubuntu VM using a [distribution which supports your desired version of MongoDB][mongodb-ubuntu-dist].
1. Attach an external volume with at least 10GB of space available, formatted to the [`fstype` mounted for `MONGODB_HOME`][mongodb-home].
   (Future work on ansible-playbooks should do this formatting for us.)
1. Ensure that your VM has at least 10GB of space available, e.g. by mounting an external formatted volume.
1. Create a `hosts` file which specifies the IP and/or DNS entry for the new VM, and designates it as a `mongodb`
   service.

   Opencrafters, see [ansible-secrets/hosts] for an example.

   E.g.

        [mongodb:children]
        mongodb-openedx

1. Create a yaml file under `host_vars/` which defines the ansible variables for your service.

   See [mongodb.yml] for the roles that will be run, and those roles for specific variables to override.

   OpenCrafters, see [ansible-secrets/host_vars] for a working examples of standalone and clustered services.

   e.g. `host_vars/mongodb-openedx.example.com.yml`:

        ---
        FORWARD_MAIL_SMTP_USER: mongodb-openedx
        FORWARD_MAIL_SMTP_PASSWORD: PASSWORD-GOES-HERE

        mongodb_version: "4.0.23"
        mongodb_root_admin_name: "root"
        mongodb_root_admin_password: PASSWORD-GOES-HERE
        mongodb_user_admin_name: "user_admin"
        mongodb_user_admin_password: PASSWORD-GOES-HERE
        mongodb_users:
            - name: 'user_admin'
              password: PASSWORD-GOES-HERE
              roles: 'userAdminAnyDatabase,dbAdminAnyDatabase'
              database: 'admin'

        MONGODB_OPENSTACK_DB_DEVICE: "/dev/vdb1"


1. Follow the instructions on [ansible-playbooks/README] to set up the ansible playbook submodules.

1. Provision the mongo service using ansible:

        ansible-playbook -vvv deploy/playbooks/deploy-all.yml --limit mongodb-openedx
1. [Verify that the mongo instance/cluster is working and monitored](#verify-mongo).

## Upgrade mongo

Mongodb.com provides instructions for upgrading instances between versions.

See for example:
* [Upgrade a Standalone to 4.0][mongodb-4.0-standalone]. This will cause an outage to your service.
* [Upgrade a Replica Set to 4.0][mongodb-4.0-replicaset]. No outage is required for a replicaset cluster.

Once you've ensured your instance meets the prerequisites, you can use ansible to perform the upgrade.

1. Take a backup of your instance's data. See [MongoDB Backup Methods][mongodb-backup] for details.
1. Update the `mongodb_version` in your `host_vars/` yaml file.
1. Run the `ansible-playbook` command to reprovision mongo on each node.

If you're upgrading a replicaset, upgrade each node one at a time.

1. Use `--limit` when running `ansible-playbook` to first upgrade each secondary node, one at a time.
1. From one of the running `mongodb` instances, check the status of the upgraded node from the mongodb shell:

        rs.status()

   Wait until the upgrade node reports as a healthy `SECONDARY` before proceeding to the next node.
1. Before upgrading the primary, [step down the primary][mongodb-stepdown] so that another node takes over as `PRIMARY`.

   To do this, run the following from the primary's mongo shell:

        rs.stepDown()

   After upgrading the primary, once it returns to the cluster, it will automatically be promoted back to primary.
1. Remove backwards-incompatible features by using [`setFeatureCompatibilityVersion`][mongodb-feature-compatibility]
   from the primary mongo shell.
1. [Verify that the mongo instance/cluster is working and monitored](#verify-mongo).

## Verify mongo

After provisioning or upgrading mongo, check that all the nodes are behaving as expected.

1. If you deployed a replicaset, check that the secondaries are in sync by running:

        rs.printSecondaryReplicationInfo()

1. Check that MongoDB specific monitoring is working as expected. Ensure that the `mongodb-exporter` service is running,
   and exporter is running and showing as `UP` on the [prometheus targets page][prometheus-targets].


[FAL-871]: https://tasks.opencraft.com/browse/FAL-871
[FAL-872]: https://tasks.opencraft.com/browse/FAL-872
[FAL-1777]: https://tasks.opencraft.com/browse/FAL-1777
[openedx_native.yml]: https://github.com/edx/configuration/blob/master/playbooks/openedx_native.yml
[mongodb-backup]: https://docs.mongodb.com/manual/core/backups/
[koa-mongodb]: https://github.com/edx/configuration/blob/open-release/koa.3/playbooks/openedx_native.yml#L60-L61
[lilac-mongodb]: https://github.com/edx/configuration/blob/open-release/lilac.master/playbooks/openedx_native.yml#L89-L90
[mongodb-ubuntu-dist]: http://repo.mongodb.org/apt/ubuntu/dists/
[mongodb-home]: https://github.com/open-craft/ansible-playbooks/blob/master/playbooks/roles/mongodb/tasks/main.yml#L14-L19
[mongodb.yml]: https://github.com/open-craft/ansible-playbooks/blob/master/playbooks/mongodb.yml
[ansible-secrets/hosts]: https://github.com/open-craft/ansible-secrets/tree/master/hosts
[ansible-secrets/host_vars]: https://github.com/open-craft/ansible-secrets/tree/master/host_vars
[ansible-playbooks/README]: https://github.com/open-craft/ansible-playbooks#submodules
[mongodb-4.0-standalone]: https://docs.mongodb.com/manual/release-notes/4.0-upgrade-standalone/
[mongodb-4.0-replicaset]: https://docs.mongodb.com/manual/release-notes/4.0-upgrade-replica-set/
[mongodb-stepdown]: https://docs.mongodb.com/manual/reference/method/rs.stepDown/#mongodb-method-rs.stepDown
[mongodb-feature-compatibility]: https://docs.mongodb.com/manual/reference/command/setFeatureCompatibilityVersion/#mongodb-dbcommand-dbcmd.setFeatureCompatibilityVersion
[prometheus-targets]: https://prometheus.net.opencraft.hosting/targets#job-mongodb-exporter
