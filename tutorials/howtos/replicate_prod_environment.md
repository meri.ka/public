Replicating an OpenEdx Production Environment
=============================================

Purpose
-------

The purpose of this document is to provide a simple list of steps to replicate
the production environment of a client.

Introduction
------------

This might be necessary when debugging an OpenEdx production instance issue
related to instance data and therefore hard to replicate in a test environment.
Instead of poking around the client production instance (and data), we must
replicate the environment into another that is safe to poke and mess in any way
necessary to debug the issues.

Prepare a clone of the production SQL database
----------------------------------------------

This is probably the most important step in this document, and largely
depend on where the production database is currently provisioned.

### AWS RDS - Amazon Relational Database Service

If the production database is running in the [Amazon RDS](https://console.aws.amazon.com/rds/home),
you can provision another RDS database based on a snapshot in time of the
original one.

Here, you'll navigate to Amazon RDS management console, select the database,
click `Actions` and `Restore to point in time`.

![Restore to point in time screenshot](./images/awsrds1.png)

Next, pick the point in time you want to restore and provision a new
database instance with this data.

![Pick point in time screenshot](./images/awsrds2.png)

### MySQL

For a standard MySQL deployment, the process requires a few more steps.

First, provision another MySQL server to host a copy of the production
database. Ensure to respect any client rules on geography and ensure the
security enforcement of this server is the same as the production
environment.

After that, you'll need to get production data in that environment. One way
to achieve that is performing an online dump and import, e.g:

```bash
replica:~$ ssh user@host "mysqldump -u user -p database | gzip -c" | gunzip | mysql -u root -p database
```

Also, we have [MySQL backups](../ops/backups.md) stored for a number of
points in time, from daily to weekly and monthly, which can be used for
provisioning the replica server. This would relieve the production server
of dumping the whole database, as it has been already performed for the
backups.

Production-like sandbox
-----------------------

This is just necessary if you need the UI to trigger and debug the issue.

Here you can use a Devstack or an OCIM sandbox.

All you need is to provision the correct version of the environment and
ensure it replicates the behavior of the production instance. For now,
it means provisioning a sandbox or devstack based on Ironwood.

Make sure to modify the settings for the LMS and other services to use
the replicated database at `/edx/etc/[lms,studio,ecommerce].yml` or
`/edx/app/edxapp/[lms,cms]_auth.json`, depending
on the OPENEDX_RELEASE used for provisioning.

If using the devstack, you can modify
these directly. Otherwise, if using OCIM, edit the instance settings before
deployment so it's deployed with the correct database settings.

![OCIM MySQL configuration screenshot](./images/ocim-mysql.png)

Course-data (MongoDB)
---------------------

If specific course data is required, the simplest way to achieve that is
exporting the course from the production source and import it into the
test environment using Open edX Studio.

![Import Export Menu screenshot](./images/export-import-1.png)

This is much simpler than setting up a MongoDB database/cluster and
restoring data.

First, access Studio in the production instance and export the course.

![Import Export Menu screenshot](./images/export-import-2.png)

Next, in your replica environment, create a new course and import the data exported from production.

![Import Export Menu screenshot](./images/export-import-3.png)
