# edX AWS Deployment Tutorial

Purpose: This tutorial will walk you through deploying edX to [Amazon Web Services](https://aws.amazon.com). Although we will only be deploying one application server in this tutorial, this kind of setup enables deploying multiple application servers and/or replacing existing application servers without any downtime.

We will be using [Ansible](https://www.ansible.com/) to automate deployment. We will be using external MySQL ([RDS](https://aws.amazon.com/rds)) and MongoDB ([Atlas](https://www.mongodb.com/cloud)) databases in order to provide better scalability and fault tolerance.

<br><br><br>

--------------

**TODO**: These instructions need to be replaced with Terraform code templates. All new AWS infrastructure should be defined and managed by Terraform.

Do not manually provision any new servers using the steps below! Instead please do it using Terraform and then replace these instructions with the new Terraform-based instructions. 
If you don't have time/budget to fully use Terraform, then please at least partially use Terraform and make sure each new deployment is using more Terraform than the last. 
See [the Terraformed Infrastructure epic](https://tasks.opencraft.com/browse/SE-2436) for details.

There is already a guide for AWS Resources Setup using [terraform here](AWS_terraform_deployment_tutorial.md), 
which should replace the resources setup sections only, for installation (ansible scripts, etc.)
you should still follow this guide. 

--------------

<br><br><br>

Prerequisites
=============

AWS Account
-----------

If you don't have an AWS account yet, [sign up now](https://aws.amazon.com).

MongoDB Atlas Account
---------------------

You will also need a [MongoDB Atlas account](https://www.mongodb.com/cloud).
There is a shared OpenCraft Account that can be used when setting up client
instances. You can find the credentials for that in the vault [here](https://vault.opencraft.com:8200/ui/vault/secrets/secret/show/core/OpenCraft%20Tools%20:%20Resources%20:%20MongoDB%20Atlas).

IAM Role Sign On From OpenCraft Account
=======================================

If this is a new AWS account for a new client, you will want to set up access to it from the main AWS OpenCraft account as this allows all OpenCraft contractors to access the account.

Go to [roles](https://console.aws.amazon.com/iam/home#/roles) in the new account and click "Create role". Fill it out like this:

* Select type of trusted entity: Another AWS account
* Account ID: The ID of the target AWS account.
* Require external ID (Best practice when a third party will assume this role): False
* Require MFA: True

Attach permissions policies: AdministratorAccess
Set permissions boundary: Create role without a permissions boundary

Role name: OpenCraft_Admin_Access

General Security considerations
===============================

* Each "flavor" of instances (edxapp, director, etc.) should have its own ssh keypair.

AWS Resources
=============

We will create an EC2 instance, an RDS instance, and an S3 bucket on
Amazon Web Services. We will be using instance types and settings that
are a good starting point for mid-traffic edX installation. Depending
on your use case, you may need to tweak the values to better suit your
specific needs.

If edX analytics pipeline and dashboard are planned for this setup, more
resources will be used -- See [AWS Analytics Setup](AWS_analytics_Setup.md#aws-resource).

Each "flavor" of instances (edxapp, director, etc.) should have its own ssh keypair.

AWS Region
----------

Select the region that is best for the client (typically `us-east-1` or `eu-west-1`).
It's best to pick one of the regions supported by MongoDB Atlas (see the
pricing page at https://www.mongodb.com/cloud/atlas/pricing).

EC2 Instance
------------

We will be using 2 [EC2](https://aws.amazon.com/ec2) instances: a large
instance called `edxapp` to host the django and ruby edX applications (LMS,
CMS, and the forums), and a micro instance called `director`, from which to run
the ansible setup playbooks for `edxapp`.

Log into the AWS console and go to the EC2 Dashboard. Choose your preferred
region using the drop-down in the top right corner.

For each of the two instances, click the *Launch Instance* button on the EC2
Dashboard, and follow the steps to configure the instance.  Most other
configuration steps you can leave at their default values, unless specified
below.

### Step 1. Choose AMI

We will be using the *Ubuntu 16.04 LTS (Xenial Xerus)* operating
system which is the officially supported system of the Open edX
platform. We will need to locate the ID of the most recent official
*Ubuntu 16.04* AWS image.

Go to https://cloud-images.ubuntu.com/locator/ and search for version
`16.04` `amd64` `ebs-ssd` instance in your preferred AWS region,
for example `us-east-1`. If `ebs-ssd` is not available in your
region, pick `hvm-ssd` instead. If there are multiple AMIs available,
select the most recently released one. Copy the AMI ID of the image you
selected (it will look something like `ami-d8132bb0`).

Choose the *Community AMIs* tab and paste the AMI ID into the search box. The
AWS image corresponding to that ID should show up. Click the *Select* button.

### Step 2. Choose Instance Type

For the `edxapp` instance, select the General Purpose `t2.large` instance type.

For the `director` instance, select the `t2.micro` instance type.

### Step 3. Configure Instance

Ensure the Network setting is set to the default VPC, *not* EC2-Classic.

### Step 4. Add Storage

For the `edxapp`, increase the root volume size to at least 50 GB.

For the `director` instance, the root volume can remain at the default size of 8 GB.

If there are any other volumes besides `Root` listed
(`Instance Store 0`, `EBS`, etc.), you can safely remove them.

### Step 5. Tag Instance

Give it a `Name` type tag with a a value that will let you easily identify
the instance in the EC2 dashboard. The usual convention is to name your edxapp
instances `edxapp-N` where `N` starts at `1` and is incremented each time
you deploy a new instance.

The director instance is most often simply named `director`.

### Step 6. Configure Security Group

For both instances, create a new security group, and name it with the instance
name, i.e. `edxapp` or `director`.

The `edxapp` group should have three rules defined:

* `SSH`, port `22`, source `director` security group
* `HTTP`, port `80`, source `Anywhere` (used to access the LMS)
* `HTTPS`, port `443`, source `Anywhere` (used to access the LMS over https)
* `Custom TCP Rule`, port `18010`, source `Anywhere` (used to access the CMS)
* `Custom TCP Rule`, port `19100`, source `Anywhere` (used by Prometheus to scrape the Node Exporter)
* `Custom TCP Rule`, port `8301 - 8302`, source `Anywhere` (used by Consul gossip)
* `Custom UDP Rule`, port `8301 - 8302`, source `Anywhere` (used by Consul gossip)

The `director` group should have one rule defined:

* `SSH`, port `22`, source `Anywhere`

Click the *Launch* button. A popup will come up that will ask you to
select or create a key pair that you will use to access your
instance. If you don't already have an existing key pair that you want
to use, select *Create a new key pair* and give it a recognizable
name. Click *Download Key Pair* and store the downloaded `.pem` file
in a safe location (See *Sensitive Data* below).

Click the *Instances* button to see the new instance.  Ensure that each EC2
instance is added to the `default` security group, and the security group created
for that particular instance.  To add an EC2 instance to a security group,
click *Actions*, then *Networking > Change Security Groups*, and check the
appropriate checkboxes.

After the instance is fully initialized, SSH into it using the key
file you used when creating this instance:

    ssh -i path/to/keyfile.pem ubuntu@ec2-xx-xx-xx-xx.compute-1.amazonaws.com

Install all available *Ubuntu 16.04* updates with:

    sudo apt-get update && sudo apt-get -y upgrade

Elastic IP
----------

We will create an Elastic IP and assign it to our EC2 instance. An
Elastic IP makes it possible to perform updates or replace servers
without downtime. When a new server is ready, you can simply switch
the Elastic IP to point to the new server.

Click the *Elastic IPs* link In the EC2 dasboard (located under
*Network & Security*). Click the *Allocate New Address* button, select
EC2, and click *Yes, Allocate*.

Select the new Elastic IP from the list, and click the *Actions ->
Associate Address*. Select your EC2 instance from the dropdown and click
*Associate*.

Your EC2 instance can now be reached through the Elastic IP.

RDS Instance
------------

We will be using an [RDS](https://aws.amazon.com/rds) instance to
host the MySQL database used by the main django applications.

Navigate to the RDS Dashboard from you AWS console. Got to "Instances"
and click the *Launch DB Instance* button to create a new instance.
Choose `MySQL` from the Engine Selection list. In the next step do NOT
choose Multi-AZ and Provisioned IOPS Storage, unless you know you need
those.

On the following pages, make the settings given below.  Everything
that's not listed can be left at the default value.

Setting name                   | value
-------------------------------|-------------------------------
DB engine version              | 5.6.x (pick the latest version)
DB Instance Class              | db.m3.medium
Multi-AZ Deployment            | No
Storage Type                   | General Purpose (SSD)
Allocated Storage              | 5 GB
DB Instance Identifier         | arbitrary, e.g. "edxapp"
Master Username/Password       | arbitrary, write them down
Publicly Accessible            | No
VPC Security Group(s)          | default (VPC)
Database Name                  | **leave empty**
Enable Encryption              | yes
Backup Retention Period        | 14 days
Auto Minor Version Upgrade     | No


By leaving the Database Name field empty, no initial database will
get created. We'll create the databases below. Leave other values at their
defaults.

Click the *Launch DB Instance* button if everything is set.

Once the RDS instance is set up, it should be accessible from the EC2 instance.
Test this by shelling into each EC2 instance and typing the following, using
the RDS endpoint host name:

    telnet xxxxxxxxx.rds.amazonaws.com 3306

Confirm that you see a "Connected to ..." message. Type Ctrl-D to exit the
telnet shell.

S3 Buckets
----------

### Edxapp Storage

Files uploaded to the edX forums, grade reports generated by the Instructor
Dashboard, and files generated by the [xqueue external grading
service](https://github.com/edx/xqueue) are stored to an
[S3](https://aws.amazon.com/s3) bucket.

We will first create a special AWS user account that we will grant
permissions to write and read from our bucket.

Go to the IAM dashbard. Go to the *Policies* tab. Click the *Create
policy* button and select the *JSON* tab on the next page.

Paste this into the *JSON* textarea, replacing `BUCKET_NAME` with the name of
your bucket:

    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": "s3:*",
          "Resource": [
            "arn:aws:s3:::BUCKET_NAME",
            "arn:aws:s3:::BUCKET_NAME/*"
          ]
        }
      ]
    }

Click *Review policy*.

On the next page, give the policy a recognizable name, and click *Create policy*.

Now navigate to the *Users* tab. Click the *Add user* button. Pick a
name for the account, for example `edxapp_s3`. Select *Programmatic
access* access type and click *Next: Permissions*. Click *Attach
existing policies directly*, search for the policy you created above,
and select it. Click *Next: Review*, then *Create user* on the final
step.

Write down *Access key ID* and *Secret access key* that are shown on
the next page, then click *Close* to go back to the *Users* section.

Navigate to the S3 section of the AWS console. Click the *Create
Bucket* button to create a new bucket. Give the bucket a recognizable
name, and set the region to the region where your EC2 instance is
hosted. Keep clicking *Next* until you reach the final step and create
the bucket.

Set following variables in your secure `vars.yml` file:

* `AWS_ACCESS_KEY_ID`: new access key ID
* `AWS_SECRET_ACCESS_KEY`: new access secret
* `EDXAPP_AWS_STORAGE_BUCKET_NAME`: name of the storage bucket

#### ORA2

To allow [edx-ora2](https://github.com/edx/edx-ora2) to upload files to the S3 bucket, the CORS configuration needs to
be updated.

See [edx-ora-2.readthedocs.io](http://edx-ora-2.readthedocs.io/en/latest/architecture/fileupload.html#configuration) for
details.

### Tracking Logs

Tracking logs generated by edxapp will be stored to a separate bucket.

As above, create a separate user, with an access key and secret, and a policy
that grants full access to this bucket.  Name the bucket something like
`client-name-tracking-logs`.

Set following variables in your secure `vars.yml` file:

* `AWS_S3_LOGS_ACCESS_KEY_ID`: new access key ID
* `AWS_S3_LOGS_SECRET_KEY`: new access secret
* `COMMON_OBJECT_STORE_LOG_SYNC_BUCKET`: name of the tracking logs bucket

MongoDB Atlas Database
----------------------

We will need a MongoDB database for the main django app and the forums.

The client will need to create a new MongoDB Atlas account, set up an
organisation in that account, create a project within that organisation and
invite you as user with enough access to set up everything you need. The client
should set up their payment details so that you can launch a new cluster.

The client should invite ops@opencraft.com to their new  Organisation as an
"Organization Member" at least. Then should also include `ops` as a
"Project Owner" or "Project Cluster Manager" for their project to be able to
create and administer a new cluster. The credentials for the `ops` account on
MongoDB Atlas are available [here]([vault-atlas-ops](https://vault.opencraft.com:8200/ui/vault/secrets/secret/show/core/OpenCraft%20Tools%20:%20Resources%20:%20MongoDB%20Atlas)).

Log into the Atlas account, navigate to the correct Organisation and project,
and click the *Build a New Cluster* button on the interface to launch a new
cluster.

Select the *same AWS region* in which the client's instance is deployed. For the
Cluster tier you will need one which lets you use MongoDB 3.4 since newer
versions don't work properly with the platform. Currently this means selecting
a M10 cluster or greater.

Under *Additional Settings* select MongoDB 3.4, and make sure *Turn on Backup*
is on. "Cloud Provider Snapshots" should be sufficient for this use case since
only the Forum is modified continuously, and "Continuous" backups are
significantly more expensive. It might be worth discussing the options with the
client.

Some clients might want encryption enabled, you might need to perform additional
steps to do that, and as of this writing, Encryption at rest is not compatible
with "Continuous" backups.

Give the Cluster a useful name, and click *Create Cluster*. It should now being
launching.

Meanwhile you can start configuring *Database Access* and *Network Access*. Lets
start with *Network Access*.

By default the new cluster is inaccessible to the outside network, you will need
to manually set up access to allow your cluster to be accessible by the Open edX
servers. There are two ways to do this, either by whitelisting the IP address of
the AWS servers, or by creating a Peering Connection. The Peering Connection is
highly recommended.

Click on *Network Access* under *Security* and visit the *Peering* tab. Here
click on the button labelled *NEW PEERING CONNECTION* and follow the
instructions presented.

Next let's set up *Database Access* which allows us to create credentials to
actually access the database from edxapp and the forum. Start by visiting
*Database Access* under *Security*.

To add a user you can click on the *ADD NEW USER* button. Choose a
username/password combination and write it down somewhere. Do not use any
reserved URL characters (`; / ? : @ = &`) in your password or you will run into
issues when trying to start the forum service. You can just autogenerate a
password from the UI.

Under *User Privileges* click on *Add Default Privileges*, select the
"readWrite" role and for the database name enter `edxapp`. This will be the name
we use later for the edxapp database. You can choose a different name, but this
is the convention. Leave *Collection* empty and click on *Add User*.

Do the same for the forum user, which should have access to the `edxforum`
database with "readWrite" permission.

Note that these configuration details are shared for all clusters in this
project!

Now we need the connection string so we can extract the relevant details from
it. For this you can just click on the *CONNECT* button on the Cluster in the
*Clusters* view. We just need the connection string so click on "Connect with
the Mongo Shell", then "I have Mongo Shell installed" and select "3.4 or
earlier". You shuld get a mongo string like the following:

    mongodb://{cluster-name}-shard-00-00-gc7jt.mongodb.net:27017,{cluster-name}-shard-00-01-gc7jt.mongodb.net:27017,{cluster-name}-shard-00-02-gc7jt.mongodb.net:27017/{database}?replicaSet={Cluster-name}-shard-0

Note down the server names, and the replicaset name.

MongoDB Atlas only supports creating users in the `admin` database and using
that for an authentication source. This isn't supported by the edx-platform and
forum service as of this writing. Based on what branches this deployment is
supposed it might be important to incorporate changes from the following three
PRs:

- [edx-platform#20819](https://github.com/edx/edx-platform/pull/20819): This
  PR adds support for specifying an authentication database to edx-platform.
- [cs_comments_service#275](https://github.com/edx/cs_comments_service/pull/275):
  This PR adds support for specifying an authentication database to the forum
  service.
- [configuration#5195](https://github.com/edx/configuration/pull/5195): This
  PR adds support for specifying an authentication database for edx-platform and
  the forum service during configuration.

For a regular deployment it is enough to use the opencraft-release/* branch for
edx-platform, configuration **and cs_comments_service**.

Here is the configuration options you will need to provide:

```yaml
# Main mongo DB
EDXAPP_MONGO_HOSTS: '<comma separated list of mongo cluster hosts without port number>'
EDXAPP_MONGO_PORT: 27017
EDXAPP_MONGO_DB_NAME: 'edxapp'
EDXAPP_MONGO_USER: 'edxapp'
EDXAPP_MONGO_PASSWORD: '<password for edxapp user here>'
EDXAPP_MONGO_USE_SSL: yes
EDXAPP_MONGO_REPLICA_SET: '<replica set name here>'
EDXAPP_MONGO_AUTH_DB: 'admin'

# Forum mongo DB
FORUM_MONGO_USE_SSL: true
FORUM_MONGO_AUTH_DB: 'admin'
FORUM_MONGO_PASSWORD: '<password for edxforum user here>'
FORUM_MONGO_USER: 'edxforum'
FORUM_MONGO_DATABASE: 'edxforum'
FORUM_MONGO_REPLICA_SET: '<replica set name here>'
FORUM_MONGO_HOSTS:
  - <uri of server 1 without port>
  - <uri of server 2 without port>
  - <uri of server 3 without port>
# This needs to be respecified because the regular configuration doesn't support
# specifying a replicaset name.
FORUM_MONGO_URL: "mongodb://{{ FORUM_MONGO_USER }}:{{ FORUM_MONGO_PASSWORD }}@{%- for host in FORUM_MONGO_HOSTS -%}{{ host }}:{{ FORUM_MONGO_PORT }}{%- if not loop.last -%},{%- endif -%}{%- endfor -%}/{{ FORUM_MONGO_DATABASE }}?replicaSet={{ FORUM_MONGO_REPLICA_SET }}"
```

YouTube Data API
----------------

Set up a dedicated Google account to get a YouTube Data API browser key for the
edxapp instance to use.

Logout of Google (or open incognito browser window), and visit
https://accounts.google.com

Click on "Create account", and select "I prefer to use my current email
address".  Use an alias of your `@opencraft.com` email address, named with
your username and the name of the edxapp instance.  E.g., if the username is
`matt`, and the edxapp instance name is `abc`, use:

    matt+abc@opencraft.com

Create the account, and validate via email as requested.

Visit the Google Developers Console at https://developers.google.com

Create a new project in the Google Developers Console:
https://console.developers.google.com/

Go to the APIs overview section and click the *+ Enable API*
button. Select *YouTube Data API*, then click the *> Enable* button.

Click the *Create Credentials* button to add API credentials to your
project. Look for the following text: "If you wish you can skip this
step and create an API key, client ID, or service account" and click
the *API key* link.

Enter a recognizable name, and select *HTTP referrers (web sites)
under *Key restriction*. Enter appropriate wildcard pattern to only
and allow requests from the hostname for your edxapp instance.  E.g.,
if the instance URL is www.abc.com, allow requests from:

    *.abc.com/*

Copy the generate access key and set it to the
`EDXAPP_YOUTUBE_API_KEY` variable in your `vars.yml` file.

Director Setup
==============

Ansible Setup
-------------

We'll be running the ansible playbooks from the the EC2 `director` instance
created above.

To do this, we'll need to install some basic bootstrapping packages:

    ssh -i path/to/keyfile.pem ubuntu@ec2-xx-xx-xx-xx.compute-1.amazonaws.com

    # Install git, pip, and build libraries
    sudo apt-get install git python-pip python-dev build-essential libssl-dev libffi-dev

    # Install mysql client and dev packages
    sudo apt-get install libmysqlclient-dev mysql-client

    # Disable ssh-agent forwarding to the director
    # This command line relies on the option already being mentioned in a file,
    # at least in a comment, which it is on standard Debian or Ubuntu installations.
    sudo sed -i '/AllowAgentForwarding/c\AllowAgentForwarding no' /etc/ssh/sshd_config
    # If it isn't actually there, then make sure to `sudo vim /etc/ssh/sshd_config` and add that line.

We want to have the latest git to be able to use some features like specifying the SSH command
through the config:

    sudo add-apt-repository ppa:git-core/ppa -y
    sudo apt-get update
    sudo apt-get install git -y

You will probably want to create a dedicated python virtualenv, for example:

    sudo pip install virtualenv virtualenvwrapper
    mkdir ~/virtualenvs
    echo "export WORKON_HOME=~/virtualenvs" >> ~/.bashrc
    echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc
    echo "export PIP_VIRTUALENV_BASE=~/virtualenvs" >> ~/.bashrc
    source ~/.bashrc

We will be basing our deployment on the `openedx_native.yml` playbook
from the edX [configuration](https://github.com/edx/configuration)
repository, so clone the repository:

    git clone git@github.com:edx/configuration.git

Install the required python packages:

    cd configuration
    mkvirtualenv edx-configuration
    pip install -r pre-requirements.txt
    pip install -r requirements.txt

This will install ansible among other dependencies.

Sensitive Data
--------------

We will store our sensitive data, such as database passwords, in a
separate location out of the public git repository. Create a directory
that will hold the sensitive data together with other custom variables
somewhere on the disk, for example:

    mkdir /home/edx/edx-secure-config

Typically, the `edx-secure-config` folder will contain at least three files:

* `vars.yml` - file containing ansible variables that will override
  default values playbooks/roles from the `configuration`
  repository, including sensitive information such as database
  passwords.
* `edxapp.pem` - your AWS certificate you downloaded when setting up
  the edxapp EC2 instance.
* `director.pem` - your AWS certificate you downloaded when setting up
  the director EC2 instance.

The `vars.yml` file is where you configure your edX installation. It
contains variables that define things such as hostnames and ports, as
well as sensitive information, such as MySQL and MongoDB access
details. Look at the yml files in OpenCraft IM for an example of
variables you need to configure. Note that the configuration
repository changes regularly, so you might need to alter this
configuration file depending on the version of edX you use:

* [common ansible variables](https://github.com/open-craft/opencraft/tree/master/instance/templates/instance/ansible) -
  you will have to combine variables from these yaml files.
* [secret keys](https://github.com/open-craft/opencraft/blob/master/instance/models/mixins/secret_keys.py) -
  generate random keys for all variables defined in that file and
  check the translation table to make sure certain keys match each other.

The secure configuration repository needs to be checked out on the director
instance.  To this end, create a Github deploy key with read and write
permissions for the private repository, and use the deploy key to clone the
repository on the director instance.  This way, everyone who can log into the
director automatically has permission to push and pull the private
configuration.

You can create the deploy key on the director with:

    ssh-keygen -t rsa -b 4096 -C "ops@opencraft.com"

Let the name remain as `id_rsa`. Finally, although this isn't required when using `id_rsa`, you can do this to explicitly specify the key when inside of the secure repository on the director instance:

    git config core.sshCommand "ssh -i ~/.ssh/id_rsa"

Now take the public key from this (`id_rsa.pub`) and apply it as a Deploy Key on the secret repository (both GitHub and GitLab support this feature).

Ecommerce Configuration (Optional)
----------------------------------

Put your ecommerce extra vars in the site's secure repo `vars.yml`.  See
[vars.yml](https://github.com/open-craft/opencraft/tree/master/instance/templates/instance/ansible/vars.yml)
and
[mysql.yml](https://github.com/open-craft/opencraft/tree/master/instance/templates/instance/ansible/mysql.yml)
for the `ECOMMERCE_*` variables that should be set.

To enable ecommerce, ensure:

```yaml
    ENABLE_SANDBOX_ECOMMERCE: true
```

Ensure that the payment processors are configured.  The default
`ECOMMERCE_PAYMENT_PROCESSOR_CONFIG` stanza uses CyberSource and PayPal, and
configures them using `ECOMMERCE_CYBERSOURCE_*` and `ECOMMERCE_PAYPAL_*`, so if
you only need these payment processors, you can simply set the appropriate
variables (see
[vars.yml](https://github.com/open-craft/opencraft/tree/master/instance/templates/instance/ansible/vars.yml)]).

However, if you need to add a custom payment processor, you'll need to provide
the full `ECOMMERCE_PAYMENT_PROCESSOR_CONFIG` stanza.  See [Stripe](#stripe)
for details.

Please also note that ecommerce requires: OAuth2 provider to be enabled so
you should add `ENABLE_OAUTH2_PROVIDER: true` to `FEATURES`. Also OAuth2
by default requires a `https://` connection, which is not true for some
of our instances (notably: sandboxes) so you'll need to set
`EDXAPP_OAUTH_ENFORCE_SECURE: false` in this case.

### Git repo

If you're using the [edx/ecommerce.git](https://github.com/edx/ecommerce.git)
repo, then it's enough to set `ECOMMERCE_VERSION` to the appropriate
upstream branch/changeset.

But if you're using a client fork, you need to override the full
`ECOMMERCE_REPOS` stanza, using your client org name as the `PATH`.

```yaml
ECOMMERCE_REPOS:
  - PROTOCOL: "{{ COMMON_GIT_PROTOCOL }}"
    DOMAIN: "{{ COMMON_GIT_MIRROR }}"
    PATH: "{{ COMMON_GIT_PATH }}"  # override this
    REPO: ecommerce.git
    VERSION: "{{ ECOMMERCE_VERSION }}"
    DESTINATION: "{{ ecommerce_code_dir }}"
    SSH_KEY: "{{ ECOMMERCE_GIT_IDENTITY }}"
```

### Stripe (Optional, for eCommerce)

To use the Stripe payment processor, merge the changes from
[ecommerce PR #601](https://github.com/edx/ecommerce/pull/601) into your instance's ecommerce
fork.  This change has been tested with [edx-platform
release-2016-03-30](https://github.com/edx/edx-platform/tree/release-2016-03-30).
See
[BehavioralInsightsTeam:release-bit](https://github.com/BehavioralInsightsTeam/ecommerce/tree/release-bit)
for an deployed example, merged with
upstream@[811190](https://github.com/BehavioralInsightsTeam/ecommerce/commit/81119065dfdb3c08dd7dfbc1d0777d89099aaff5).

Add this stanza to your `vars.yml`, and update it using the live [Stripe
keys](https://dashboard.stripe.com/account/apikeys).

```yaml
ECOMMERCE_PAYMENT_PROCESSOR_CONFIG:
  edx:
    stripe:
      publishable_key: 'pk_live_xxxxxxxxxxxxxxxxxxxxxxxx'
      secret_key: 'sk_live_xxxxxxxxxxxxxxxxxxxxxxxx'
      image_url: '/static/images/default-theme/logo.png'
      receipt_path: '/commerce/checkout/receipt/'
      cancel_path: '/commerce/checkout/cancel/'
      error_path: '/commerce/checkout/error/'
```

Creating the MySQL databases
----------------------------

The ansible playbooks do not create the required MySQL databases.  Create them
by connecting to the RDS MySQL database create above from the `director`
instance, using the mysql client:

    mysql -h xxxxxxxxx.rds.amazonaws.com -u edxapp -p
    mysql> create database edxapp default character set utf8;
    mysql> create database edxapp_csmh default character set utf8;
    mysql> create database xqueue default character set utf8;
    mysql> create database notes_api default character set utf8;

If you're [enabling ecommerce](#ecommerce-configuration), then also create the
ecommerce database and read-write user on the edxapp RDS instance:

    mysql -h xxxxxxxxx.rds.amazonaws.com -u edxapp -p
    mysql> create database ecommerce default character set utf8;
    mysql> CREATE USER 'ecommerce'@'%' IDENTIFIED BY 'ecommerce_password';
    mysql> GRANT ALL PRIVILEGES ON `ecommerce`.* TO 'ecommerce'@'%';

Running the Playbook
--------------------

Our deployment is based on the `edx_sandbox` playbook from the
`configuration` repository. To run the playbook, go to the
`playbooks` folder inside the `configuration` repository. It is
important to run the playbook from inside the `playbooks` folder, so
that extra ansible configuration in `playbooks/ansible.cfg` that the
edX playbooks depend on gets picked up by ansible:

    cd configuration/playbooks
    workon edx-configuration   # activate virtualenv, if required

(Optional) If this setup will have analytics VM, apply `configuration` [patches][opencraft-config-patches]
(commit range: [bf7ff53][commit-range-start]..[f6d1813][commit-range-end])

[opencraft-config-patches]: https://github.com/open-craft/configuration/commits/analytics-sandbox
[commit-range-start]: https://github.com/open-craft/configuration/commit/bf7ff53108db2395d2f436d8f5280550409f7fa9
[commit-range-end]: https://github.com/open-craft/configuration/commit/f6d18136446bedd597cfa119b3ab774d3356cec7

Run the playbook like this, where 123.123.123.123 is the private IP of the new VM:

    ansible-playbook -i 123.123.123.123, \
                     -e @/path/to/vars.yml \
                     --user=ubuntu \
                     --private-key=/path/to/edxapp.pem \
                     edx_sandbox.yml

Once the playbook finishes running (it can take a long time!), your
edX instance should be all set up and running.

Additional Services
-------------------

We run sidecar services like Filebeat, Consul, and the Prometheus Node Exporter on
all of our infrastructure servers, and have adapted them to work on Open edX appservers
as well, whether on OpenStack or on AWS.

For that reason, deploying these services is one last step that needs to be done before
we can consider the Open edX appserver fully provisioned from our side.

### Setup

These steps need only be run once on the director instance secure repo, to set up the playbooks and variables.

1. Add a deploy key to https://github.com/open-craft/ansible-common-secrets under the director instance.

        ssh-keygen -t rsa -b 4096 -C "ops@opencraft.com" -f ~/.ssh/opencraft-ansible-common-secrets-deploy-key

2. Go into the secure configuration directory.
3. Copy over [ansible.cfg](resources/ansible.cfg).
4. Copy over [hosts](resources/hosts) -- note that there are a few variables in there that need replacing, and there may be groups (like `services` and the stage ones) which you do not need and can safely delete. If the client only requires a single instance, then you just need one entry under the `openedx-app-prod` group.
5. `git submodule add https://github.com/open-craft/ansible-playbooks.git deploy`
6. We need to do this just once to initially clone the private submodule:

        ssh-agent bash -c 'ssh-add ~/.ssh/opencraft-ansible-common-secrets-deploy-key; git submodule add git@github.com:open-craft/ansible-common-secrets.git group_vars'
        git submodule update --init --recursive

7. Specify the key to use for this submodule so that we can easily run `git pull` in the future.

        cd group_vars
        git config core.sshCommand "ssh -i ~/.ssh/opencraft-ansible-common-secrets-deploy-key"
        cd ..

8. Commit these new submodules and versions to the secure repo.

9. Create a virtualenv to run the playbook:

        mkvirtualenv oc-ansible --python=python3

### Running the playbook

Run these steps on the director instance to add additional services to new appserver(s).

1. Update the `hosts` file to reflect the new appserver(s) public and private IP addresses. In the case of servers behind elastic ip, the public address should be the public elastic ip address, not the public instance address.
1. Update the playbooks and dependencies:

        workon oc-ansible
        git submodule update --remote --merge
        pip install -Ur deploy/requirements.txt

1. Run the playbook.

     Make sure to use the right parameters for any of the above steps, e.g. maybe your `vars-production.yml` file is
     just `vars.yml`, or the private key name is different.

        # Be sure you've first updated the `hosts` file!
        ansible-playbook -vv deploy/playbooks/appserver.yml -l openedx-app-prod --private-key=edxapp.pem -e @vars-production.yml

After the playbook succeeds, you'll have our additional services successfully installed on the appserver(s).

If you ran this on a single appserver and now plan on taking an image of it and reproducing it to put 2 or more behind a load balancer, you *will need to run this playbook again*.

The reason is that the image would contain certain variables, like the VM's private and public IP, which would also get copied over. Running the `appserver.yml` playbook from the last step above one more time, this time on all new appservers copied from an image, will ensure their configurations are up-to-date.

### Consul Configuration
When using AWS auto-scaled deployments, the `consul_auto_generate_config` variable should be set to ``true`` in the inventory file, in addition to the existing `consul_ip`, `consul_nodename` variables. This is because when an instance is launched from an AMI, its IP addresses are likely to be different. Setting ``consul_auto_generate_config`` to ``true`` will ensure that consul IP address and node name are discovered and configured dynamically during the consul service startup. The instance's IP addresses are discovered from the local EC2 metadata API and a unique consul nodename is generated by joining public and private ip with the value of `consul_nodename` in the format `<consul_nodename value>A-B-C-D-W-X-Y-Z`, where `A.B.C.D` is the instance's private IP address and `W.X.Y.Z` is the instance's public IP address. As this only works at the consul service startup, this will do nothing if the instance's IP address is changed at runtime.

### Troubleshooting

After running this playbook, verify that the services are running as expected.

* Consul: run `sudo consul members` on the instance, and grep the members list to ensure your new server is `alive`.
  Run `sudo service consul status` on the instance to check the consul service.
  Note that [consul membership is distributed](https://www.consul.io/docs/internals/architecture.html#10-000-foot-view),
  and so eventual consistency will be reached across the network, so long as the [`8301-8302` ports are open on the
  security group](#step-6-Configure-Security-Group).
* Monitoring: Login to https://prometheus.net.opencraft.hosting/targets and ensure your service is `UP`.
  If your instance is not showing up, check the node exporter by running `sudo service nodeexporter status` on the
  instance.
  You may need to reload `nginx` and check that the [`19100` port is open on the security
  group](#step-6-Configure-Security-Group) so that prometheus can poll the node exporter service.
* ELK: Login to https://logs.opencraft.com and filter the logs to ensure your new server's logs are present.
  Use the instance's `hostname` as the `host`, e.g. `host: "ip-172-31-27-50" AND type: "edxapp"`.
  Run `sudo service filebeat status` to check the status of this service.

Configuring Ecommerce (Optional)
================================

Some manual steps are required to set up the ecommerce site.

[The official edX
documentation](http://edx.readthedocs.io/projects/edx-installing-configuring-and-running/en/latest/ecommerce/install_ecommerce.html)
describes the setup for devstack, but it's still useful.

Commerce Configuration
----------------------

Using a superuser account, login to client LMS `/admin/commerce/commerceconfiguration/`.

Ensure that there is a Commerce Configuration added, with:

* Enabled: On
* Checkout on ecommerce service: On
* checkout URL: /basket/single-item (the default)

Partner site configuration
--------------------------

Replace the `$VARIABLES` in the command below with their values from your `vars.yml`.

```bash
ubuntu@edxapp-N:~$ sudo -u ecommerce -s
ecommerce@edxapp-N:~$ cd /edx/app/ecommerce/ecommerce
ecommerce@edxapp-N:/edx/app/ecommerce/ecommerce$ source ../ecommerce_env
ecommerce@edxapp-N:/edx/app/ecommerce/ecommerce$ ./manage.py create_or_update_site \
                --site-id=1 \
                --site-domain=$EDXAPP_LMS_BASE \
                --partner-code=edX \
                --partner-name='$EDXAPP_PLATFORM_NAME' \
                --lms-url-root='$EDXAPP_LMS_BASE_SCHEME://$EDXAPP_LMS_BASE' \
                --theme-scss-path=sass/themes/edx.scss \
                # comma-delimited list of payment processors
                --payment-processors='stripe,paypal,' \
                --client-id=$ECOMMERCE_SOCIAL_AUTH_EDX_OIDC_KEY \
                --client-secret=$ECOMMERCE_SOCIAL_AUTH_EDX_OIDC_SECRET
```

Enable Payment Processor
------------------------

If you're using the [Stripe payment processor](#stripe), you need to switch it
on:

```bash
ubuntu@edxapp-N:~$ sudo -u ecommerce -s
ecommerce@edxapp-N:~$ cd /edx/app/ecommerce/ecommerce
ecommerce@edxapp-N:/edx/app/ecommerce/ecommerce$ source ../ecommerce_env

# Lists the current switches
ecommerce@ip-172-31-26-74:/edx/app/ecommerce/ecommerce$ ./manage.py switch -l

# activate stripe
ecommerce@ip-172-31-26-74:/edx/app/ecommerce/ecommerce$ ./manage.py switch payment_processor_active_stripe on --create
```

Deploying Analytics VM
======================

See [AWS Analytics Setup](AWS_analytics_setup.md) for details on how to deploy the analytics stack.

Verifying the instance
======================

Once the instance is deployed, got through [the checklist](../../checklists/manual_instance_test.md)
to make sure it works correctly.

An alternative [extended test checklist](https://handbook.opencraft.com/en/latest/test_checklist/) is also available.

CloudWatch monitoring
=====================

AWS provides EC2 and RDS monitoring along with alarms that send notifications whenever a certain preconfigured threshold on resources is reached. See [CloudWatch Setup](cloudwatch_setup.md) for instructions to set it up.

New Relic Monitoring
====================

Once the instance is known to be working correctly, set up New Relic Synthetics monitors
for the LMS and Studio home page URLs.

To create a new New Relic account, go to https://newrelic.com/signup and fill out the details. Note that you can create multiple accounts with the same email address and you will easily be able to switch between multiple accounts in the New Relic UI (open the dropdown in the top right corner and you will see a *Switch account* option.

Under *Account settings -> Active Users*, add `xavier@opencraft.com` and `braden@opencraft.com` as admins.

To set up New Relic monitoring, follow the guide from [newrelic](../../ops/newrelic.md).

Make sure that the notification channels for the alert policy (for any outages) are set to both `urgent@opencraft.com` and `ops@opencraft.com` so that the on-call team members will be paged when an outage happens. Additionally, any client-specific email addresses can be added if required.
